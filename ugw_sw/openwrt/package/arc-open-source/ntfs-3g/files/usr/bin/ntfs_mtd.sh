#!/bin/sh
#
# Copyright (C) 2009 Arcadyan
# All Rights Reserved.
#
# FTP configuration shell program
#
#
# $1 - device node, e.g. /dev/sda1, /dev/sdb2
# $2 - NTFS version of partition
# $3 - label name
#

NTFS_MTD_CFG="/tmp/ntfs_mtd.cfg"

# add by Garnet 2016/10/4
if [ "X$1" != "X" ] ; then
  DEVICENAME=`echo $1 | cut -c 6-`
  DEVICENAME_BASE=`echo $DEVICENAME | cut -c 1-3`
  DEVICENAME_INDEX=`echo $DEVICENAME | cut -c 4`
fi

if [ "X${DEVICENAME_INDEX}" == "X" ] ; then
  DEVICENAME_INDEX=0
fi
# end by Garnet 2016/10/4

if [ -z "$2" ] ; then
	exit 1
fi

if ! [ -f "${NTFS_MTD_CFG}" ] ; then
	echo "!" > ${NTFS_MTD_CFG}
fi

# Modify by Garnet for ALDK platform

# debug
#echo "DEVICENAME=${DEVICENAME}, BASE=${DEVICENAME_BASE}, INDEX=${DEVICENAME_INDEX}" > /dev/console
#echo "1=$1 , 2=$2, 3=$3" > /dev/console

#ccfg_cli -f ${NTFS_MTD_CFG} set version@$1=$2
abscfg set ARC_USB_DISKLABEL_${DEVICENAME_BASE}_x_Version ${DEVICENAME_INDEX} $2

# Simon@2013/01/04, usb double quote to do label name
#ccfg_cli -f ${NTFS_MTD_CFG} set label@$1="$3"
abscfg set ARC_USB_DISKLABEL_${DEVICENAME_BASE}_x_Label ${DEVICENAME_INDEX} $3

#end by Garnet
