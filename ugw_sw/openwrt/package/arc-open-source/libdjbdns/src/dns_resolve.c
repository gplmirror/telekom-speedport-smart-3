#include "iopause.h"
#include "taia.h"
#include "byte.h"
#include "dns.h"
#include "ip6.h"
#include <sys/types.h>
#include <unistd.h>

#if 1
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <stdio.h>

#define FONAP_RUN_DIR   "/tmp/fonap"
#define FONAP_LOG_SIZE  100000
#define FONAP_LOG_FLAG  FONAP_RUN_DIR "/fonap_debug_flag"
#define FONAP_FONSMCD_DEBUG_LOG_FILE    FONAP_RUN_DIR "/fonsmcd_debug.log"

#define FONAP_DEBUG(fmt, arg...) do {\
  FILE *fp; \
  struct stat stat_buf; \
  if ((stat(FONAP_LOG_FLAG, &stat_buf) == 0) && S_ISREG(stat_buf.st_mode)) { \
    if ((stat(FONAP_FONSMCD_DEBUG_LOG_FILE, &stat_buf) == 0) && (stat_buf.st_size < FONAP_LOG_SIZE)) { \
      fp=fopen(FONAP_FONSMCD_DEBUG_LOG_FILE, "a"); \
    } else { \
      fp=fopen(FONAP_FONSMCD_DEBUG_LOG_FILE, "w"); \
    } \
    if (fp) { \
      FILE *uptime_fp; \
      unsigned long up_s = 0; \
      unsigned long up_ns = 0; \
      unsigned long id_s = 0; \
      unsigned long id_ns = 0; \
      uptime_fp=fopen("/proc/uptime", "r"); \
      if (uptime_fp) { \
        fscanf(uptime_fp, "%lu.%02lu %lu.%02lu", &up_s, &up_ns, &id_s, &id_ns); \
        fclose(uptime_fp); \
      } \
      fprintf(fp, "[uptime:%lu.%02lu]: pid=[%d] ", up_s, up_ns, getpid()); \
      fprintf(fp, fmt, ## arg); \
      fprintf(fp, "\n"); \
      fclose(fp); \
    } \
  } \
} while (0)
#else
#define FONAP_DEBUG(fmt, arg...)
#endif

struct dns_transmit dns_resolve_tx = {0};

int dns_resolve(const char *q,const char qtype[2], const int ipversion, const int timeout)
{
  struct taia stamp;
  struct taia deadline;
  char servers4[64];
  char servers6[256];
  iopause_fd x[1];
  int r;

  if (ipversion == 4) {
    if (dns_resolvconfip(servers4) == -1) return -1;
    if (dns_transmit_start(&dns_resolve_tx,servers4,1,q,qtype,"\0\0\0\0") == -1) return -1;
  } else if (ipversion == 6) {
    if (dns_resolvconfip6(servers6) == -1) { FONAP_DEBUG("%s(%d)", __func__, __LINE__); return -1; }
    if (dns_transmit6_start(&dns_resolve_tx,servers6,1,q,qtype,V6any) == -1) { FONAP_DEBUG("%s(%d)", __func__, __LINE__); return -1; }
  } else { FONAP_DEBUG("%s(%d)", __func__, __LINE__); return -1; }

  for (;;) {
    taia_now(&stamp);
    /* FIXME: hue modified to adjust timeout seconds */
    //taia_uint(&deadline,120);
    taia_uint(&deadline,timeout);
    taia_add(&deadline,&deadline,&stamp);
    dns_transmit_io(&dns_resolve_tx,x,&deadline);
    iopause(x,1,&deadline,&stamp);
    if (ipversion == 4) {
      r = dns_transmit_get(&dns_resolve_tx,x,&stamp);
    } else if (ipversion == 6) {
      r = dns_transmit6_get(&dns_resolve_tx,x,&stamp);
    } else {
      r = -1;
    }
    if (r == -1) return -1;
    if (r == 1) return 0;
  }
}
