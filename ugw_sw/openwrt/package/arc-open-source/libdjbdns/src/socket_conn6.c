#include <sys/types.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include "byte.h"
#include "socket.h"

int socket_connect6(int s,const char ip6[16],uint16 port,uint32 scope_id)
{
  struct sockaddr_in6 sa;

  byte_zero(&sa,sizeof sa);
  sa.sin6_family = AF_INET6;
  uint16_pack_big((char *) &sa.sin6_port,port);
  byte_copy((char *) &sa.sin6_addr,16,ip6);
  sa.sin6_scope_id = scope_id;

  return connect(s,(struct sockaddr *) &sa,sizeof sa);
}

int socket_connected6(int s)
{
  struct sockaddr_in6 sa;
  int dummy;
  char ch;

  dummy = sizeof sa;
  if (getpeername(s,(struct sockaddr *) &sa,&dummy) == -1) {
    read(s,&ch,1); /* sets errno */
    return 0;
  }
  return 1;
}
