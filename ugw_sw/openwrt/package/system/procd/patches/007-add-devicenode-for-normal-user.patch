--- a/plug/hotplug.c
+++ b/plug/hotplug.c
@@ -33,9 +33,12 @@
 
 #include "../procd.h"
 
+#include "../service/instance.h"
+
 #include "hotplug.h"
 
 #define HOTPLUG_WAIT	500
+#define MAX_LEN	64
 
 struct cmd_handler;
 struct cmd_queue {
@@ -157,6 +160,14 @@ static void handle_makedev(struct blob_a
 				ERROR("cannot set group %s for %s\n",
 					blobmsg_get_string(tb[2]),
 					blobmsg_get_string(tb[0]));
+		} else {
+			char dev_node[MAX_LEN] = {0};
+			sscanf(blobmsg_get_string(tb[0]), "/dev/%s", dev_node);
+			struct service_instance *in = check_for_devicenode_existence(dev_node, NULL);
+			if(in) {
+				if (chown(blobmsg_get_string(tb[0]), in->uid, in->gid) < 0)
+					ERROR(" cannot set group for device node %s with uid %d gid %d (%s)\n", blobmsg_get_string(tb[0]), in->uid, in->gid, strerror(errno));
+			}
 		}
 	}
 	umask(oldumask);
--- a/service/instance.c
+++ b/service/instance.c
@@ -58,6 +58,7 @@ enum {
 	INSTANCE_ATTR_PIDFILE,
 	INSTANCE_ATTR_RELOADSIG,
 	INSTANCE_ATTR_TERMTIMEOUT,
+	INSTANCE_ATTR_DEVICENODES,
 	__INSTANCE_ATTR_MAX
 };
 
@@ -84,6 +85,7 @@ static const struct blobmsg_policy insta
 	[INSTANCE_ATTR_PIDFILE] = { "pidfile", BLOBMSG_TYPE_STRING },
 	[INSTANCE_ATTR_RELOADSIG] = { "reload_signal", BLOBMSG_TYPE_INT32 },
 	[INSTANCE_ATTR_TERMTIMEOUT] = { "term_timeout", BLOBMSG_TYPE_INT32 },
+	[INSTANCE_ATTR_DEVICENODES] = { "devicenodes", BLOBMSG_TYPE_ARRAY },
 };
 
 enum {
@@ -372,6 +374,23 @@ instance_run(struct service_instance *in
 		dup2(_stderr, STDERR_FILENO);
 		closefd(_stderr);
 	}
+	if (in->devicenodes) {
+
+		char dev_node[MAX_LEN];
+		struct stat s;
+		blobmsg_for_each_attr(cur, in->devicenodes, rem) {
+
+			if (snprintf(dev_node, sizeof(dev_node), "/dev/%s", blobmsg_get_string(cur))) {
+				if (stat(dev_node, &s))
+					ERROR("%s: not setting group as device node %s is not available\n", in->name, dev_node);
+				else {
+					if (chown(dev_node, in->uid, in->gid) < 0)
+						ERROR(" cannot set group for device node %s with uid %d gid %d (%s)\n", dev_node, in->uid, in->gid, strerror(errno));
+				}
+			}
+    	}
+	}
+
 
 	if (in->uid && capabilities) {
 		if (drop_capabilities(in->capabilities, &usercaps))
@@ -601,11 +620,29 @@ instance_exit(struct uloop_process *p, i
 void
 instance_stop(struct service_instance *in)
 {
+	struct blob_attr *cur;
+	int rem;
 	if (!in->proc.pending)
 		return;
 	in->halt = true;
 	in->restart = in->respawn = false;
 	kill(in->proc.pid, SIGTERM);
+	if (in->devicenodes) {
+
+		char dev_node[MAX_LEN];
+		struct stat s;
+		blobmsg_for_each_attr(cur, in->devicenodes, rem) {
+
+			if (snprintf(dev_node, sizeof(dev_node), "/dev/%s", blobmsg_get_string(cur))) {
+				if (stat(dev_node, &s))
+					ERROR("%s: not setting group as %s is missing\n", in->name, dev_node);
+				else {
+					if (chown(dev_node, 0, 0) < 0)
+						ERROR(" cannot set group for device node %s with uid %d gid %d (%s)\n", dev_node, 0, 0, strerror(errno));
+				}
+			}
+    	}
+	}
 	uloop_timeout_set(&in->timeout, in->term_timeout * 1000);
 }
 
@@ -762,6 +799,64 @@ instance_fill_array(struct blobmsg_list
 	return true;
 }
 
+struct service_instance *check_for_devicenode_existence(const char *devnode, struct service_instance *cur_in)
+{
+	struct service *s;
+	struct service_instance *in;
+	struct blob_attr *cur;
+	int rem, flag;
+
+	avl_for_each_element(&services, s, avl) {
+		if (!avl_is_empty(&s->instances.avl)) {
+			if((flag = (cur_in?strcmp(cur_in->srv->name, s->name):1))) {
+				vlist_for_each_element(&s->instances, in, node) {
+					if (in->devicenodes) {
+						blobmsg_for_each_attr(cur, in->devicenodes, rem) {
+							if (!strcmp(blobmsg_get_string(cur),devnode)) {
+								ERROR("%s: devicenode %s is owned by service %s\n", __func__, blobmsg_get_string(cur), s->name);
+								return in;
+							}
+						}
+					}
+				}
+			}
+		}
+	}
+	return NULL;
+}
+
+static bool
+instance_config_parse_devicenodes(struct service_instance *in, struct blob_attr **tb)
+{
+	struct blob_attr *cur, *cur2;
+	bool ret = false;
+	int rem;
+
+	cur = tb[INSTANCE_ATTR_DEVICENODES];
+	if (!cur) {
+		in->devicenodes = NULL;
+		return true;
+	}
+
+	if (!blobmsg_check_attr_list(cur, BLOBMSG_TYPE_STRING)) {
+		return false;
+	}
+
+	blobmsg_for_each_attr(cur2, cur, rem) {
+		ret = true;
+		break;
+	}
+
+	blobmsg_for_each_attr(cur2, cur, rem) {
+		if (check_for_devicenode_existence(blobmsg_get_string(cur2), in)) {
+			in->devicenodes = NULL;
+			return true;
+		}
+	}
+
+	in->devicenodes = cur;
+	return ret;
+}
 static int
 instance_jail_parse(struct service_instance *in, struct blob_attr *attr)
 {
@@ -929,6 +1024,11 @@ instance_config_parse(struct service_ins
 			in->capabilities = capabilities;
 	}
 
+	if (!instance_config_parse_devicenodes(in, tb)){
+		ERROR("%s: Changing the ownership for the devicenodes ignored\n", in->name);
+		return false;
+	}
+
 	if (tb[INSTANCE_ATTR_PIDFILE]) {
 		char *pidfile = blobmsg_get_string(tb[INSTANCE_ATTR_PIDFILE]);
 		if (pidfile)
@@ -1131,6 +1231,9 @@ void instance_dump(struct blob_buf *b, s
 	if (in->capabilities)
 		blobmsg_add_string(b, "capabilities", in->capabilities);
 
+	if (in->devicenodes)
+		blobmsg_add_blob(b, in->devicenodes);
+
 	if (in->pidfile)
 		blobmsg_add_string(b, "pidfile", in->pidfile);
 
--- a/service/instance.h
+++ b/service/instance.h
@@ -73,6 +73,7 @@ struct service_instance {
 
 	struct blob_attr *command;
 	struct blob_attr *trigger;
+	struct blob_attr *devicenodes;
 	struct blobmsg_list env;
 	struct blobmsg_list data;
 	struct blobmsg_list netdev;
@@ -87,5 +88,6 @@ void instance_update(struct service_inst
 void instance_init(struct service_instance *in, struct service *s, struct blob_attr *config);
 void instance_free(struct service_instance *in);
 void instance_dump(struct blob_buf *b, struct service_instance *in, int debug);
+struct service_instance *check_for_devicenode_existence(const char *devnode, struct service_instance *cur_in);
 
 #endif
