#ifndef _ARC_OSIX_H_
#define _ARC_OSIX_H_

#ifndef __KERNEL__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <signal.h>
#include <dirent.h>
#include <sched.h>
#include <pthread.h>
#include <asm/unistd.h>
#endif

typedef char            BOOLEAN_T;
typedef signed char     INT1;
typedef unsigned char   UINT1;
typedef char            BOOL1;
typedef signed short    INT2;
typedef unsigned short  UINT2;
typedef signed int      INT4;
// typedef unsigned int    UINT4;
typedef unsigned long    UINT4;  // For 64 bits concern
typedef unsigned long ULONG;  // Maybe we should check which one should use ULONG, not UINT4
typedef signed char  CHR1;
#define  VOID           void    
typedef float            FLT4;

typedef struct tMacAddress {
    UINT4  u4Dword;
    UINT2  u2Word;
} tMacAddress;

#define MAC_ADDR_LEN 6
typedef UINT1 tMacAddr [MAC_ADDR_LEN];

/* Undefine names that are defined in standard system headers. */
#undef PRIVATE
#undef PUBLIC
#undef VOLATILE
#undef EXPORT

#ifndef NULL
#define NULL    (0)
#endif

#if !defined(PRIVATE)
#define PRIVATE static
#endif

#if !defined(VOLATILE)
#define VOLATILE volatile
#endif

#if !defined(PUBLIC)
#define PUBLIC  extern
#endif

#if !defined(FALSE)  || (FALSE != 0)
#define FALSE  (0)
#endif

#if !defined(TRUE) || (TRUE != 1)
#define TRUE   (1)
#endif

#ifndef EXPORT
#define EXPORT
#endif

#define   UNUSED_PARAM(x)   ((void)x)

/* For driver writers, and in case you are using longjmp etc. */
typedef volatile signed char     VINT1;
typedef volatile unsigned char   VUINT1;
typedef volatile signed short    VINT2;
typedef volatile unsigned short  VUINT2;
typedef volatile signed int      VINT4;
typedef volatile unsigned long   VUINT4;

#define  OSHAL_CHECK_MSG_WITH_HIT                    (0)
#define  OSHAL_CHECK_MSG_WITH_NONE                   (-1)
#define  OSHAL_CHECK_MSG_WITH_TIMEOUT                (-2)
#define  OSHAL_CHECK_MSG_WITH_KTHREAD_STOP           (-3)

#ifndef __KERNEL__
#ifdef ARC_OSIX_LIB_DEBUG_MSG
    #define ARC_OSIX_LIB_DMSG(fmt,args...) do{printf("\nDBG(%s.%d):" fmt, __FUNCTION__,__LINE__, ##args);}while(0);
#else
    #define ARC_OSIX_LIB_DMSG(fmt,args...) do{}while(0);
#endif

#ifdef ARC_OSIX_LIB_WARNING_MSG
    #define ARC_OSIX_LIB_WMSG(fmt,args...) do{printf("\nWARN(%s.%d):" fmt, __FUNCTION__, __LINE__,##args);}while(0);
#else
    #define ARC_OSIX_LIB_WMSG(fmt,args...) do{}while(0);
#endif
#endif



#define ARC_OSIX_WRAPPER_DEV    "/dev/"_ARC_KERNEL_OSIX_DEV_NAME 
#define ARC_CREATE_OSIX_DEVICE_NODE(x)    do{ system("/usr/sbin/char_dev_node_create.sh "(x));}while(0);  

#define ARC_OSIX_FREE(x)    if(x) {free((x));};

#define MAX_NO_OF_EVENT_SUPPORT            (64)
#define MAX_NO_OF_SEMAPHORE_SUPPORT        (128)
#define MAX_NO_OF_MESSAGE_QUEUE_SUPPORT    (64)
#define MAX_NO_OF_TMR_SUPPORT               (96)


#define MAX_MESSAGE_UNIT_SIZE       (2048)
#define MAX_MESSAGE_UNITS_PER_QUEUE    (16)

#define ARC_KERNEL_OSIX_MMAP_SIZE    (((sizeof(ARC_OSIX_GLOBAL_INFO_CTRL_T)/4096)+1)*4096)

#if 0
#define ARC_OSIX_RESERVE_MMAP_FOR_MSG_QUEUE_SPACE_AREA   ((( (MAX_NO_OF_MESSAGE_QUEUE_SUPPORT* \
																MAX_MESSAGE_UNIT_SIZE* \
																MAX_MESSAGE_UNITS_PER_QUEUE)/4096)+1)*4096)
#else
/* For the mesage content, always do twice copy, otherwise  not so easy to maintain the memory allocation. */
/* In case we just create the queue and did not  delete the queue, it might be easier to maintain the queue */
#define ARC_OSIX_RESERVE_MMAP_FOR_MSG_QUEUE_SPACE_AREA   (0)
#endif

#define ARC_OSIX_INVALID_ID_NUMBER         ((UINT4) (-1))

typedef enum{
	OSIX_RESOURCE_TYPE_SEMAPHORE=0,
	OSIX_RESOURCE_TYPE_EVENT,
	OSIX_RESOURCE_TYPE_QUEUE,
	OSIX_RESOURCE_TYPE_TIMER,
	OSIX_RESOURCE_TYPE_TASK,
}OSIX_RESOURCE_TYPE_ENUM;

#define OSIX_CHECK_MSG_WITH_HIT            (OSHAL_CHECK_MSG_WITH_HIT)
#define OSIX_CHECK_MSG_WITH_NONE           (OSHAL_CHECK_MSG_WITH_NONE)
#define OSIX_CHECK_MSG_WITH_TIMEOUT        (OSHAL_CHECK_MSG_WITH_TIMEOUT)
#define OSIX_CHECK_MSG_WITH_KTHREAD_STOP   (OSHAL_CHECK_MSG_WITH_KTHREAD_STOP)

/* Data Structure Definition */
#define MAX_LEN_OF_EVENT_NAME  (8)
typedef struct arc_osix_event_info_ctrl_s {
    UINT4 event_id;
    UINT4 event_mask;
    UINT4 event_result;
    INT1 event_name[MAX_LEN_OF_EVENT_NAME+1];   
}ARC_OSIX_EVENT_INFO_CTRL_T;

typedef struct arc_osix_event_ioctl_s{
    UINT4 event_id;
    UINT4 event_mask;
    UINT4 event_data;
    UINT4 timeout;
    INT4   ret_cause;  /* OSIX_CHECK_MSG_WITH_TIMEOUT, OSIX_CHECK_MSG_WITH_KTHREAD_STOP, OSIX_CHECK_MSG_WITH_HIT */
}ARC_OSIX_EVENT_IOCTL_T;


#define MAX_LEN_OF_SEMAPHORE_NAME  (8)
typedef struct arc_osix_semaphore_info_ctrl_s {
    UINT4 semaphore_id;
	UINT4 balance_ctr; /* balance_ctr */
	int   init_count;  /* Init Semaphore Counter */
    INT1 sema_name[MAX_LEN_OF_SEMAPHORE_NAME+1];
}ARC_OSIX_SEMAPHORE_INFO_CTRL_T;

#define MAX_LEN_OF_MESSAGE_QUEUE_NAME  (8)
typedef struct arc_osix_msg_queue_info_ctrl_s {
    UINT4 msgq_id;
    /* VOID *realKernelMessageQueue;   Real Data Source in the Queue, It would be pointed by the allocation, (msg_unit_size*num_of_units) */
    INT4 msg_unit_size;     /* Boundary Size, refer MAX_MESSAGE_UNIT_SIZE  */
    INT4 no_of_units;         /* Boundary Size, refer MAX_MESSAGE_UNITS_PER_QUEUE  */
    INT4 unprocessed_items;  /* Number of unprocessed messages */
    INT4 produce_index;         /* Produce Index */
    INT4 consume_index;         /* Consume Index */
    /* VOID *mmap_start_addr;  Unused now */ /* Start address for the real message store */
    INT1 msgq_name[MAX_LEN_OF_MESSAGE_QUEUE_NAME+1];
}ARC_OSIX_MSG_QUEUE_INFO_CTRL_T;

typedef struct arc_osix_msg_queue_ioctl_s{
	INT4 qindex;
	VOID *puser_msg;
	UINT4 user_msg_size;  /* Useless later */
	UINT4 timeout;	
       INT4 ret_cause;  /* OSIX_CHECK_MSG_WITH_TIMEOUT, OSIX_CHECK_MSG_WITH_KTHREAD_STOP, OSIX_CHECK_MSG_WITH_HIT */
}ARC_OSIX_MSG_QUEUE_IOCTL_T;

/* Timer Definition */
typedef int (*OSIX_USER_SPACE_EXPIRE_FUNCTION_TYPE)(void *user_arg);

#define MAX_LEN_OF_TMR_NAME  (8)
#define ARC_OSIX_TMR_EXPIRE_FLAG_ON   (1)
#define ARC_OSIX_TMR_EXPIRE_FLAG_OFF   (0)

#define ARC_OSIX_TMR_IS_NOT_START      (0)
#define ARC_OSIX_TMR_IS_READY          (1)
#define ARC_OSIX_TMR_IS_READY_TO_DESTROY   (2)
#define ARC_OSIX_TMR_IS_UNUSED        (3)
#define ARC_OSIX_TMR_IS_OCCUPIED_BY_OTHER_APPLICATION  (4)

typedef struct arc_osix_tmr_info_ctrl_s {
    INT4  pid;  /* So far, we use the timer task to handle each timer individually */
    VOID *pchild_thread_info; /* OSIX_THREAD_MGR_INFO_T */
    UINT4 timeout_interval;   /* Unit is second */
    UINT4 previous_expired_tmr_stamp;  /* To make sure the expire function should execute or not */
    /* Expire Function could be the same, but without input parameters */
    OSIX_USER_SPACE_EXPIRE_FUNCTION_TYPE pUserExpireFun;
    VOID  *pUserArgumet;
    UINT4 expire_flag;
    UINT4 status_flag;
    UINT4 signal_number;
    INT1 tmr_name[MAX_LEN_OF_TMR_NAME+1];   
}ARC_OSIX_TMR_INFO_CTRL_T;


typedef struct arc_osix_global_info_ctrl_s {
    UINT4 arc_osix_dirty_flag;
    UINT4 g_arc_osix_fd;
    UINT4 g_osix_service_thread_active;

    UINT4 g_protect_event_resource_semaphore;
    UINT4 g_protect_sema4_resource_semaphore;
    UINT4 g_protect_msgqueue_resource_semaphore;
    UINT4 g_protect_tmr_resource_semaphore;

    ARC_OSIX_EVENT_INFO_CTRL_T pgVar_Event_Info_Ctrl[MAX_NO_OF_EVENT_SUPPORT];
    ARC_OSIX_SEMAPHORE_INFO_CTRL_T pgVar_Semaphore_Info_Ctrl[MAX_NO_OF_SEMAPHORE_SUPPORT];
    ARC_OSIX_MSG_QUEUE_INFO_CTRL_T pgVar_MSG_Queue_Info_Ctrl[MAX_NO_OF_MESSAGE_QUEUE_SUPPORT];  
    ARC_OSIX_TMR_INFO_CTRL_T pgVar_TMR_Info_Ctrl[MAX_NO_OF_TMR_SUPPORT];	
}ARC_OSIX_GLOBAL_INFO_CTRL_T;

typedef struct  arc_osix_obtain_msg_info_s{
	unsigned int obtain_type;   // OSIX_RESOURCE_TYPE_ENUM
	unsigned long  id_idx;
	union{
		ARC_OSIX_EVENT_INFO_CTRL_T  sEvent;
		ARC_OSIX_SEMAPHORE_INFO_CTRL_T  sSemaphore;
		ARC_OSIX_MSG_QUEUE_INFO_CTRL_T  sMsgQueue;		
	}union_data;
}ARC_OSIX_OBTAIN_MSG_INFO_T;

#endif
