#ifndef _ARC_OSIX_API_H_
#define _ARC_OSIX_API_H_
// The following file would be copied from include/linux <Kernel Tree> to <staging_dir>/usr/include [ Jess Hsieh ]
#include "k_arc_kernel_callback_def.h"

/* Type definition for OSIX wrap, semaphore, message queue, task_id (process_id) */
typedef INT4      tOsixThreadId;
typedef INT4      tOsixQId;
// typedef INT4      tOsixSemId;
typedef unsigned long      tOsixSemId;
typedef INT4      tOsixEventId;
typedef INT4      tOsixTmrId;
#if 0
/* Porting from ARC Common Utility */
#define		OSIX_NULL		(0)

#define		OSIX_TRUE		(1)
#define		OSIX_FALSE		(0)
/* <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< */

typedef enum _OSIX_WRAPPER_ERRNO
{
	OSIX_SUCCESS = 0,
	OSIX_ERROR = -1,
	OSIX_RESOURCE_BUSY=-2,
	OSIX_TIMEOUT=-3,
}OSIX_WRAPPER_ERRNO;

#define OSIX_FAILURE   OSIX_ERROR
#endif
#ifndef  MODULE
#define OSIX_NTOHL(x) (UINT4)(x)
#define OSIX_NTOHS(x) (UINT2)(x)
#define OSIX_HTONL(x) (UINT4)(x)
#define OSIX_HTONS(x) (UINT2)(x)
#define OSIX_NTOHF(x) (FLT4)(x)
#define OSIX_HTONF(x) (FLT4)(x)
#endif /* MODULE (Kernel) */
#if 0
/* <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< */
typedef enum{  
	WAN_PORT_REPORT_FROM_ADSL  = 0,  
	WAN_PORT_REPORT_FROM_VDSL,  
	WAN_PORT_REPORT_FROM_ETHERNET,
	WAN_PORT_REPORT_FROM_ADMIN
} WAN_DSL_MODE_T;

typedef struct WAN_PORT_LNK_DETECTION_STATUS{
	int line_status;
	struct timespec report_time; /* System Up Kernel Time */  
	unsigned char reporter;  /* WNA_PORT_REPORT_FROM_xDSL or WNA_PORT_REPORT_FROM_ETHERNET */
#if 0
		unsigned char reserved[3]; /* Reserved for the data structure padding */
#else
		unsigned char exception_state; /* exception_state to check xDSL sub line state */
		unsigned char reserved[2]; /* Reserved for the data structure padding */
#endif
}WAN_PORT_LNK_DETECTION_STATUS_T;

/* <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< */
#endif
#if 0
#define GPIO_BUTTON_REPORT_FROM_KMODULE      0
#define GPIO_BUTTON_REPORT_FROM_WEB          1
#define GPIO_BUTTON_REPORT_FROM_TR69         2
#define GPIO_BUTTON_REPORT_FROM_TR64         3

#define GPIO_BUTTON_FACTORY_DEFAULT_BUTTON   20  /* Port 1 , BIT 4 */
#define GPIO_BUTTON_WLAN_BUTTON_PRESS        5   /* Port 0 , BIT 5 */
#define GPIO_BUTTON_PAGE_BUTTON_PRESS        22  /* Port 1 , BIT 6 */
#define GPIO_BUTTON_DECT_REGISTER_BUTTON_PRESS         6   /* Port 0 , BIT 6 */

#define OSIX_GPIO_FACTORY_RESET_DETECTION_NAME    "MQ_RST"
#define OSIX_GPIO_WLAN_ADMIN_CTRL_DETECTION_NAME  "MQ_WADM"
#define OSIX_GPIO_WLAN_TIMER_DETECTION_NAME       "MQ_WLTM"
#define OSIX_GPIO_PAGING_DETECTION_NAME           "MQ_PAGE"
#define OSIX_GPIO_DECT_REGISTER_DETECTION_NAME    "MQ_DREG"

#define ARC_OSIX_SERVICE_THREAD_RUNNING      (1)
#define ARC_OSIX_SERVICE_THREAD_HALT            (0)

#define ARC_OSIX_EXECUTE_CMD_MAX_LENGTH    (512)

#define ARC_OSIX_SERVICE_SYSCMD_THREAD_STACK_SIZE   (8*1024)
typedef struct ARC_OSIX_EXECUTE_COMMAND_S{
	unsigned long execute_result;
	unsigned long request_pid;
	unsigned long foreground;  /* Execute them in the foreground [ under OSIXSCMD ] */
	char execute_cmd[ARC_OSIX_EXECUTE_CMD_MAX_LENGTH];
}ARC_OSIX_EXECUTE_COMMAND_T;

#define ARC_OSIX_SYSCMD_QUEUE_NAME   "OSIXSCMD"
#define ARC_OSIX_SYSCMD_MAX_QUEUE_NUMBER    (64)
#define ARC_OSIX_SYSCMD_MSG_QUE_UNIT_SIZE    sizeof(ARC_OSIX_EXECUTE_COMMAND_T)

typedef struct GPIO_PORT_BUTTON_DETECTION_STATUS{
	int trigger_gpio_pin;      /* GPIO_BUTTON_FACTORY_DEFAULT_BUTTON, GPIO_BUTTON_WLAN_BUTTON_PRESS, GPIO_BUTTON_PAGE_BUTTON_PRESS, GPIO_BUTTON_WPS_BUTTON_PRESS */
	unsigned char reporter;    /* WNA_PORT_REPORT_FROM_xDSL or WNA_PORT_REPORT_FROM_ETHERNET */
	unsigned char reserved[3]; /* Reserved for the data structure padding */
}GPIO_PORT_BUTTON_DETECTION_STATUS_T;

#define GPIO_BUTTON_MESSAGE_QUEUE_UNIT_SIZE sizeof(GPIO_PORT_BUTTON_DETECTION_STATUS_T)
#define GPIO_BUTTON_MESSAGE_QUEUE_NUMBER  64
#define WLAN_TIMER_MESSAGE_QUEUE_NUMBER   4
#endif

/* API Definition */

INT4 OsixEventCrt  (INT4  g_arc_osix_fd,INT1 *event_name, UINT4 mask_bits, UINT4 init_value, tOsixEventId *eventid);
INT4 OsixEventDel (INT4  g_arc_osix_fd,tOsixEventId eventid);
INT4 OsixEventSend(INT4  g_arc_osix_fd,tOsixEventId eventid, UINT4 events);
INT4 OsixEventRecv(INT4  g_arc_osix_fd,tOsixEventId eventid, UINT4 *result, INT4 timeout); 
INT4 OsixEventObtainID(INT4  g_arc_osix_fd,INT1 *event_name, tOsixEventId *eventid);

INT4 OsixQueCrt  (INT4  g_arc_osix_fd,INT1 *qmsg_name, UINT4 msg_unit_size, UINT4 max_no_of_msgs, tOsixQId *qid);
INT4 OsixQueDel (INT4  g_arc_osix_fd,tOsixQId qid);
INT4 OsixQueSend(INT4  g_arc_osix_fd,tOsixQId qid, INT1 *pqmsg, UINT4 size);
INT4 OsixQueRecv(INT4  g_arc_osix_fd,tOsixQId qid, VOID *pqmsg, UINT4 size , INT4 timeout);  /* 0 No Timeout */
INT4 OsixQueNumMsg(INT4  g_arc_osix_fd,tOsixQId qid, INT4 *num_of_msg, int debug_flag);
INT4 OsixQueObtainID(INT4  g_arc_osix_fd,INT1 *qmsg_name, tOsixQId *qid);

INT4 OsixSemCrt(INT4  g_arc_osix_fd,INT1 *sema_name, tOsixSemId *sema_id, int init_count);
INT4 OsixSemDel(INT4  g_arc_osix_fd,tOsixSemId sema_id);
INT4 OsixSemTake(INT4  g_arc_osix_fd,tOsixSemId sema_id);
INT4 OsixSemGive(INT4  g_arc_osix_fd,tOsixSemId sema_id);
INT4 OsixSemObtainID(INT4  g_arc_osix_fd,INT1 *sema_name, tOsixSemId *sema_id);

INT4 OsixWrapperResource_Init(INT4  g_arc_osix_fd);
INT4 OsixWrapperResource_Shutdown(INT4  g_arc_osix_fd);

INT4 Osix_mSleep(INT4 g_arc_osix_fd, INT4 miliseconds);
INT4 Osix_GetSytemTicks(INT4 g_arc_osix_fd, UINT4 *systemTicks);
INT4 Osix_GetSytemHZ(INT4 g_arc_osix_fd, UINT4 *systemHz);
INT4 Osix_GetKernelTime(INT4 g_arc_osix_fd, struct timespec *pv_kernel_time);

INT4 OsixObtainDeviceId(void);
INT4 OsixReleaseDeviceId(INT4  g_arc_osix_fd);

int lib_arc_com_parse_proc_device_file_to_create_char_device_node(char *node_name, unsigned int *node_major_no);
#endif
