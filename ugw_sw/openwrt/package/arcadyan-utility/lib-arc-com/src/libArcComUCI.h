#ifndef _ARC_COM_UCI_H_
#define _ARC_COM_UCI_H_


#include <stddef.h>

#define UTIL_FILE_MAX_OPEN_INTERVAL	100	/* 100 msec */
#define UTIL_FILE_MAX_OPEN_TRIAL		100	/* try up to 100 times */

#define UTIL_FILE_LCK  0
#define UTIL_FILE_UNLCK  1

#define		UTIL_ARRAYCNT(ary)			( sizeof(ary) / sizeof( (ary)[0] ) )	/*get the element count of an array*/
#define		UTIL_ALIGN4(num) 			( ((num)+3) / 4 * 4 )					/*get a number aligned to 4*/
#define		UTIL_ALIGN4REST(num) 		( UTIL_ALIGN4(num) - (num) )			/*get the offset of a number aligned to 4*/
#define		UTIL_MIN(val1,val2)			( (val1) < (val2) ? (val1) : (val2) )	/*get the minimum value of two numbers*/
#define		UTIL_MAX(val1,val2)			( (val1) > (val2) ? (val1) : (val2) )	/*get the maximum value of two numbers*/
#define		UTIL_INRANGE(val,min,max)	( (val) >= (min) && (val) <= (max) )	/*check if a number is in the range of two values*/
#define		UTIL_OFFSETOF(dataType,memberName)	((size_t) &((dataType*)0)->memberName)	/*get the offset of member data in a data structure*/

#define		UTIL_BIT_LSHTV(val,cnt)		( (val) << (cnt) )		/*get bit-wise-left-shift of a value*/
#define		UTIL_BIT_RSHTV(val,cnt)		( (val) >> (cnt) )		/*get bit-wise-right-shift of a value*/
#define		UTIL_BIT_LSHT(val,cnt)		( (val) <<= (cnt) )		/*set bit-wise-left-shift of a value*/
#define		UTIL_BIT_RSHT(val,cnt)		( (val) >>= (cnt) )		/*set bit-wise-right-shift of a value*/

#define		UTIL_BIT_SET(val,bit)		( (val) |=  UTIL_BIT_LSHTV(0x1,bit))	/*set a certain bit of a value*/
#define		UTIL_BIT_CLEAR(val,bit)		( (val) &= ~(UTIL_BIT_LSHTV(0x1,bit)))	/*clear a certain bit of a value*/
#define		UTIL_BIT_CHECK(val,bit)		(((val) &   UTIL_BIT_LSHTV(0x1,bit)) != 0x0 )/*check if a certain bit of a value is set*/


#ifdef __cplusplus
extern "C" {
#endif
#define RET_SUCCESS				0 //SUCCESS
#define RET_FAIL_UNKNOW			1
#define RET_FAIL_PARAMS			2
#define RET_FAIL_DUPROCESS		3
#define RET_FAIL_MEDCORE		4
#define RET_FAIL_FILEOPEN		5
#define RET_FAIL_FILEOPEN_AGENT	6
#define RET_FAIL_FILELOCK		7
#define RET_FAIL_MEMALLOC		8
#define RET_FAIL_TID			9


#define LEN_SHORT_CMD	50

#define add_UCIBatchStr		fprintf

char start_UCIBatch(FILE **, char *);
void add_UCIBatch(FILE *, char *);
char end_UCIBatch(FILE **, char *, char);
char exec_UCIBatch(char *);

#ifdef __cplusplus
}
#endif


#endif /* _ARC_COM_UCI_H_ */
