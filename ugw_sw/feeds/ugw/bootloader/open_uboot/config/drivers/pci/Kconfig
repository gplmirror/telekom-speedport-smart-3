menu "PCI"

config UBOOT_CONFIG_DM_PCI
	bool "Enable driver mode for PCI"
	depends on UBOOT_CONFIG_DM
	help
	  Use driver model for PCI. Driver model is the new method for
	  orgnising devices in U-Boot. For PCI, driver model keeps track of
	  available PCI devices, allows scanning of PCI buses and provides
	  device configuration support.

config UBOOT_CONFIG_DM_PCI_COMPAT
	bool "Enable compatible functions for PCI"
	depends on UBOOT_CONFIG_DM_PCI
	help
	  Enable compatibility functions for PCI so that old code can be used
	  with CONFIG_DM_PCI enabled. This should be used as an interim
	  measure when porting a board to use driver model for PCI. Once the
	  board is fully supported, this option should be disabled.

config UBOOT_CONFIG_PCI_SANDBOX
	bool "Sandbox PCI support"
	depends on UBOOT_CONFIG_SANDBOX && UBOOT_CONFIG_DM_PCI
	help
	  Support PCI on sandbox, as an emulated bus. This permits testing of
	  PCI feature such as bus scanning, device configuration and device
	  access. The available (emulated) devices are defined statically in
	  the device tree but the normal PCI scan technique is used to find
	  then.

config UBOOT_CONFIG_PCI_TEGRA
	bool "Tegra PCI support"
	depends on UBOOT_CONFIG_TEGRA
	help
	  Enable support for the PCIe controller found on some generations of
	  Tegra. Tegra20 has 2 root ports with a total of 4 lanes, Tegra30 has
	  3 root ports with a total of 6 lanes and Tegra124 has 2 root ports
	  with a total of 5 lanes. Some boards require this for Ethernet
	  support to work (e.g. beaver, jetson-tk1).

endmenu
