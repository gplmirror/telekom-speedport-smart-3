=========================================
Prepare and compile Intel(R) UGW Software
=========================================

1. Call prepare script in the root folder to prepare the UGW clone:
	./ugw-prepare-all.sh

2. Change into the 'openwrt' folder and select a model to compile:
	./scripts/ltq_change_environment.sh switch

3. Build using 'make' command.

