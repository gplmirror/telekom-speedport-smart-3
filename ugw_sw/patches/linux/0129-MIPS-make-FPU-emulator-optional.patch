From e316186466b867fc77e1dcb01fa2084559a2860c Mon Sep 17 00:00:00 2001
From: Manuel Lauss <manuel.lauss@gmail.com>
Date: Mon, 30 Oct 2017 15:58:12 +0530
Subject: [PATCH] MIPS: make FPU emulator optional

This small patch makes the MIPS FPU emulator optional. The kernel
kills float-users on systems without a hardware FPU by sending a SIGILL.

Disabling the emulator shrinks vmlinux by about 54kBytes (32bit,
optimizing for size).

Signed-off-by: Manuel Lauss <manuel.lauss@gmail.com>
---
 arch/mips/Kconfig                    | 14 ++++++++++++++
 arch/mips/Makefile                   |  2 +-
 arch/mips/include/asm/dsemul.h       | 23 +++++++++++++++++++++++
 arch/mips/include/asm/fpu.h          |  4 +++-
 arch/mips/include/asm/fpu_emulator.h | 11 +++++++++++
 5 files changed, 52 insertions(+), 2 deletions(-)

diff --git a/arch/mips/Kconfig b/arch/mips/Kconfig
index 444b7a72edd4..f2fa01eb2069 100755
--- a/arch/mips/Kconfig
+++ b/arch/mips/Kconfig
@@ -2949,6 +2949,20 @@ config MIPS_O32_FP64_SUPPORT
 
 	  If unsure, say N.
 
+config MIPS_FPU_EMULATOR
+	bool "MIPS FPU Emulator"
+	default y
+	help
+	  This option lets you disable the built-in MIPS FPU (Coprocessor 1)
+	  emulator, which handles floating-point instructions on processors
+	  without a hardware FPU.  It is generally a good idea to keep the
+	  emulator built-in, unless you are perfectly sure you have a
+	  complete soft-float environment.  With the emulator disabled, all
+	  users of float operations will be killed with an illegal instr-
+	  uction exception.
+
+	  Say Y, please.
+
 config USE_OF
 	bool
 	select OF
diff --git a/arch/mips/Makefile b/arch/mips/Makefile
index 5b656d9a1bbe..48dc1a9c3e42 100644
--- a/arch/mips/Makefile
+++ b/arch/mips/Makefile
@@ -287,7 +287,7 @@ OBJCOPYFLAGS		+= --remove-section=.reginfo
 head-y := arch/mips/kernel/head.o
 
 libs-y			+= arch/mips/lib/
-libs-y			+= arch/mips/math-emu/
+libs-$(CONFIG_MIPS_FPU_EMULATOR)	+= arch/mips/math-emu/
 
 # See arch/mips/Kbuild for content of core part of the kernel
 core-y += arch/mips/
diff --git a/arch/mips/include/asm/dsemul.h b/arch/mips/include/asm/dsemul.h
index a6e067801f23..d388fbe3bfbd 100644
--- a/arch/mips/include/asm/dsemul.h
+++ b/arch/mips/include/asm/dsemul.h
@@ -41,6 +41,7 @@ struct task_struct;
 extern int mips_dsemul(struct pt_regs *regs, mips_instruction ir,
 		       unsigned long branch_pc, unsigned long cont_pc);
 
+#ifdef CONFIG_MIPS_FPU_EMULATOR
 /**
  * do_dsemulret() - Return from a delay slot 'emulation' frame
  * @xcp:	User thread register context.
@@ -88,5 +89,27 @@ extern bool dsemul_thread_rollback(struct pt_regs *regs);
  * before @mm is freed in order to avoid memory leaks.
  */
 extern void dsemul_mm_cleanup(struct mm_struct *mm);
+#else
+static inline bool do_dsemulret(struct pt_regs *xcp)
+{
+	return false;
+}
+
+static inline bool dsemul_thread_cleanup(struct task_struct *tsk)
+{
+	return false;
+}
+
+static inline bool dsemul_thread_rollback(struct pt_regs *regs)
+{
+	return false;
+}
+
+static inline void dsemul_mm_cleanup(struct mm_struct *mm)
+{
+
+}
+
+#endif
 
 #endif /* __MIPS_ASM_DSEMUL_H__ */
diff --git a/arch/mips/include/asm/fpu.h b/arch/mips/include/asm/fpu.h
index f06f97bd62df..f41f2387b9c5 100644
--- a/arch/mips/include/asm/fpu.h
+++ b/arch/mips/include/asm/fpu.h
@@ -227,8 +227,10 @@ static inline int init_fpu(void)
 		/* Restore FRE */
 		write_c0_config5(config5);
 		enable_fpu_hazard();
-	} else
+	} else if (IS_ENABLED(CONFIG_MIPS_FPU_EMULATOR))
 		fpu_emulator_init_fpu();
+	else
+		ret = SIGILL;
 
 	return ret;
 }
diff --git a/arch/mips/include/asm/fpu_emulator.h b/arch/mips/include/asm/fpu_emulator.h
index c05369e0b8d6..966891f71be2 100644
--- a/arch/mips/include/asm/fpu_emulator.h
+++ b/arch/mips/include/asm/fpu_emulator.h
@@ -30,6 +30,7 @@
 #include <asm/local.h>
 #include <asm/processor.h>
 
+#ifdef CONFIG_MIPS_FPU_EMULATOR
 #ifdef CONFIG_DEBUG_FS
 
 struct mips_fpu_emulator_stats {
@@ -63,6 +64,16 @@ do {									\
 extern int fpu_emulator_cop1Handler(struct pt_regs *xcp,
 				    struct mips_fpu_struct *ctx, int has_fpu,
 				    void *__user *fault_addr);
+#else	/* no CONFIG_MIPS_FPU_EMULATOR */
+static inline int fpu_emulator_cop1Handler(struct pt_regs *xcp,
+				struct mips_fpu_struct *ctx, int has_fpu,
+				void *__user *fault_addr)
+{
+	*fault_addr = NULL;
+	return SIGILL;	/* we don't speak MIPS FPU */
+}
+#endif	/* CONFIG_MIPS_FPU_EMULATOR */
+
 void force_fcr31_sig(unsigned long fcr31, void __user *fault_addr,
 		     struct task_struct *tsk);
 int process_fpemu_return(int sig, void __user *fault_addr,
